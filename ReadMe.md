# KeyboardResponder

[![Carthage Compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
![MIT license](https://img.shields.io/badge/license-MIT-lightgrey.svg)

Easy navigation between UITextFields and UITextViews.

## Features

- [x] Navigate between multiple `UITextField`s and `UITextView`s correctly maintaining `UIScrollView` insets and offsets
- [x] Customisable `inputAccessoryView` with default `UIToolbar` implementation.
- [x] Drives navigation in TheDistanceForms - an iOS framework for creating flexible forms as generic collections of user input elements - which is coming soon.
- [x] Complete [Documentation](http://thedistance.github.io/KeyboardResponder/index.html)

## Requirements

- iOS 8.0+
- Xcode 7.3
- Swift 2.2

## Installation

### Carthage

[Carthage](https://github.com/Carthage/Carthage) is the preferred dependency manager as it reduces build times during app development. KeyboardResponder has been built for Carthage. Add 
	
	github "TheDistance/KeyboardResponder"
	
to your cartfile and run
	
	carthage update KeyboardResponder
	
to build the framework. Add to it your project according to the Carthage instructions.

#### Component Libraries

Keyboard Responder is dependent on other libraries available from The Distance:

* [TheDistanceCore](https://github.com/thedistance/TheDistanceCore): Develop faster with convenience functions from The Distance.

Ensure you add these to your project alongside `KeyboardResponder`.

## Usage

### Getting Started 

KeyboardResponder has a very simple set up. 

0. Initialise a `KeyboardResponder` object
0. Set the `scrollContainer` to ensure `contentOffset` and `contentInsets` are adjusted to keep the view on screen.
0. Set the `inputAccessoryView` on the `KeyboardResponder` - when components are added to the keyboard responder there `inputAccessoryView` will be set to this.
0. Create an array of `KeyboardResponderInputType`s from `UITextField`s and `UITextView`s, assign in to the `components` property.
0. Optionally match the `textView`'s height to just fit its contents.

That's it! When any of your fields become first responder they will appear with your `inputAccessoryView` and you can navigate between them, maintaining consistent scroll offset.

	class ViewController: UIViewController {

    	@IBOutlet weak var titleField:UITextField?
    	@IBOutlet weak var nameField:UITextField?
    	@IBOutlet weak var emailField:UITextField?
   	 	@IBOutlet weak var textView:UITextView?
    	@IBOutlet weak var scrollView:UIScrollView?
    
    	var responder:KeyboardResponder!
    
    	override func viewDidLoad() {
        	super.viewDidLoad()
        
        	// 1.
        	responder = KeyboardResponder()
        	
        	// 2.
        	responder.scrollContainer = scrollView
        	
        	// 3.
        	responder.inputAccessoryView = KeyboardResponderToolbar(navigationDelegate: responder)
        	
        	// 4.
        	if let tf = titleField, tv = textView, ef = emailField, nf = nameField {
            responder.components = [.TextField(tf), .TextField(nf), .TextView(tv), .TextField(ef)]
        	}	
        
        	// 5.
        	textView?.textContainer.heightTracksTextView
    	}
    }	

To create user input forms even easier, we have developed TheDistanceForms - an iOS framework for creating flexible forms as generic collections of user input elements - which is coming soon.

### Customisation

`KeyboardResponderToolbar` is a default implementation of the `KeyboardResponderInputAccessoryView`. You can create any arbitrary `UIView` that can be used instead providing it conforms to that protocol.

To respond to any `UITextField` or `UITextView` delegate methods to add further functionality, you should assign your delegate to the `textFieldDelegate` or `textViewDelegate` properties on `KeyboardResponder`.

To customise functionality, such as preventing navigation in certain situations, you should subclass `KeyboardResponder`, remembering to call through to `super`.

## Communication

- If you have **found a bug**, open an issue.
- If you have **a feature request**, open an issue.
- If you **want to contribute**, submit a pull request.
- If you'd like to **ask a general question**, email us on <hello+keyboardresponder@thedistance.co.uk>.


