//
//  KeyboardResponderToolbar.swift
//  KeyboardResponder
//
//  Created by Josh Campion on 28/01/2016.
//  Copyright © 2016 The Distance. All rights reserved.
//

import UIKit

/**
 
 A simple `UIToolbar` that conforms to `KeyboardResponderInputAccessoryView` to provide default navigation for a `KeyboardResponder`. That protocol defines the properties to configure the titles or images for the items.
 
 */
open class KeyboardResponderToolbar: UIToolbar, KeyboardResponderInputAccessoryView {

    // MARK: KeyboardResponderInputAccessoryView Properties
    
    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var showsPrevious:Bool = true {
        didSet {
            refreshItems()
        }
    }
    
    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var showsNext:Bool = true {
        didSet {
            refreshItems()
        }
    }
    
    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var showsDone:Bool = true {
        didSet {
            refreshItems()
        }
    }
    
    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var previousImage:UIImage? {
        didSet {
            refreshItems()
        }
    }
    
    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var previousTitle:String? {
        didSet {
            refreshItems()
        }
    }
    
    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var nextImage:UIImage? {
        didSet {
            refreshItems()
        }
    }

    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var nextTitle:String? {
        didSet {
            refreshItems()
        }
    }

    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var doneImage:UIImage? {
        didSet {
            refreshItems()
        }
    }

    /// `KeyboardResponderInputAccessoryView` protocol conformance. Calls `refreshItems()` on `didSet`.
    open var doneTitle:String? {
        didSet {
            refreshItems()
        }
    }

    /// `KeyboardResponderInputAccessoryView` protocol conformance.
    open var navigationDelegate:KeyboardResponderInputAccessoryDelegate?
    
    // MARK: Properties
    
    /// Button to navigate to the previous component. This is created in and configured `createBarButtonItems()`.
    open var previousButton:UIBarButtonItem!
    
    /// Button to navigate to the next component. This is created in and configured `createBarButtonItems()`.
    open var nextButton:UIBarButtonItem!
    
    /// Button to dismiss the keyboard. This is created in and configured `createBarButtonItems()`.
    open var doneButton:UIBarButtonItem!
    
    /// Flexible space for the default layout.
    open var flexibleSpace:UIBarButtonItem!
    
    // MARK: Initialisers
    
    /// Default initialiser that creates this toolbar, sets the navigation delegate that is queried for status and is sent navigation requests, and sets the `UIBarButtonItem` properties calling `refreshItems()`.
    public init(navigationDelegate:KeyboardResponderInputAccessoryDelegate) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        self.navigationDelegate = navigationDelegate
        refreshItems()
    }
    
    /// Required initialiser. Creates the default layout but has no navigation delegate set up.
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        refreshItems()
    }
    
    // MARK: UI Configuration
    
    /**
    
     Creates `UIBarButtonItem`s for each element in the default layout based on the properties from `KeyboardResponderInputAccessoryView`. If values are set for the `previousImage`, `nextImage` and `doneImage` properties these take precedent over the equivalent title properties.
     
     - If no `doneImage` or `doneTitle` is given a `UIBarButtonItemSystem.Done` is created instead.
     - If no `previousImage` or `previousTitle` is given, the 'Navigate Before` icon from [Google Material Design icons](https://design.google.com/icons/#ic_navigate_before) is used.
     - If no `nextImage` or `nextTitle` is given, the 'Navigate Next` icon from [Google Material Design icons](https://design.google.com/icons/#ic_navigate_before) is used.
     
     The buttons have targets set to `self` and the actions are `previousTapped(_:)`, `nextTapped(_:)` and `doneTapped(_)`.
     
     - returns: A tuple of `UIBarButtonItem`s.
     
    */
    open func createBarButtonItems() -> (prev:UIBarButtonItem, next:UIBarButtonItem, done:UIBarButtonItem, space:UIBarButtonItem) {
        
        let prev:UIBarButtonItem
        let next:UIBarButtonItem
        let done:UIBarButtonItem
        let space:UIBarButtonItem
        
        if let img = previousImage {
            prev = UIBarButtonItem(image: img, style: .plain, target: self, action: #selector(KeyboardResponderToolbar.previousTapped(_:)))
        } else {
            prev = UIBarButtonItem(title: previousTitle ?? "Prev", style: .plain, target: self, action: #selector(KeyboardResponderToolbar.previousTapped(_:)))
        }
        
        if let img = nextImage {
            next = UIBarButtonItem(image: img, style: .plain, target: self, action: #selector(KeyboardResponderToolbar.nextTapped(_:)))
        } else {
            next = UIBarButtonItem(title: nextTitle ?? "Next", style: .plain, target: self, action: #selector(KeyboardResponderToolbar.nextTapped(_:)))
        }
        
        if let img = doneImage {
            done = UIBarButtonItem(image: img, style: .plain, target: self, action: #selector(KeyboardResponderToolbar.doneTapped(_:)))
        } else if let title = doneTitle {
            done = UIBarButtonItem(title: title, style: .done, target: self, action: #selector(KeyboardResponderToolbar.doneTapped(_:)))
        } else {
            done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(KeyboardResponderToolbar.doneTapped(_:)))
        }
        
        space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        return (prev, next, done, space)
    }
    
    /// Creates new `UIBarButtonItem`s, lays them out according to the `showsPrevious` and other properties, and assigns them to `self.items`.
    open func refreshItems() {
        
        let items = createBarButtonItems()
        
        previousButton = items.prev
        nextButton = items.next
        doneButton = items.done
        flexibleSpace = items.space
        
        var newItems = [UIBarButtonItem]()
        
        if (showsPrevious && previousButton != nil)  { newItems.append(previousButton) }
        if (showsNext && nextButton != nil) { newItems.append(nextButton) }
        if (flexibleSpace != nil) { newItems.append(flexibleSpace) }
        if (showsDone && doneButton != nil) { newItems.append(doneButton) }

        self.items = newItems
        
        refreshBarButtonItemStatus()
    }
    
    
    /// Should be called to set the `.enabled` poperty based on the `navigationDelegate` responses. If there is no navigation delegate, the buttons are disabled.
    open func refreshBarButtonItemStatus() {
        nextButton.isEnabled = navigationDelegate?.inputAccessoryCanGoToNext(self) ?? false
        previousButton.isEnabled = navigationDelegate?.inputAccessoryCanGoToPrevious(self) ?? false
    }
    
    // MARK: Navigation Methods
    
    /// Called from `previousButton`. Requests navigation to the previous item from `navigationDelegate`.
    @objc func previousTapped(_ sender:AnyObject?) {
        navigationDelegate?.inputAccessoryRequestsPrevious(self)
    }
    
    /// Called from `nextButton`. Requests navigation to the next item from `navigationDelegate`.
    @objc func nextTapped(_ sender:AnyObject?) {
        navigationDelegate?.inputAccessoryRequestsNext(self)
    }
    
    /// Called from `doneButton`. Requests keboard dismissal from `navigationDelegate`.
    @objc func doneTapped(_ sender:AnyObject?) {
        navigationDelegate?.inputAccessoryRequestsDone(self)
    }
}
