//
//  UIView.swift
//  KeyboardResponder
//
//  Created by Josh Campion on 26/01/2016.
//  Copyright © 2016 The Distance. All rights reserved.
//

import Foundation
import ObjectiveC

var _InputAccessoryViewKey:UInt8 = 0

/// Defines a protocol for changeing the `inputAccessoryView`. This allows generalisation of `UITextField` and `UITextView` to `SettableInputAccessoryView`s for use interchangably in `KeyboardResponder`.
public protocol SettableInputAccessoryView: class {
    
    /// The `UIView` to accessorise the keyboard for a given input view.
    var inputAccessoryView:UIView? { get set }
    
}

public extension SettableInputAccessoryView {
    
    /// Uses associated objects to save a reference to the `inputAccessoryView` of an arbitrary `SettableInputAccessoryView`.
    var inputAccessoryView:UIView? {
        get {
            return objc_getAssociatedObject(self, &_InputAccessoryViewKey) as? UIView
        }
        set {
            objc_setAssociatedObject(self, &_InputAccessoryViewKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

/// Conformance to `SettableInputAccessoryView` to use `UITextView` interchangably with `UITextField` in a `KeyboardResponder`.
extension UITextView: SettableInputAccessoryView { }

/// Conformance to `SettableInputAccessoryView` to use `UITextField` interchangably with `UITextView` in a `KeyboardResponder`.
extension UITextField: SettableInputAccessoryView { }
