//
//  KeyboardResponderInputAccessoryDelegate.swift
//  KeyboardResponder
//
//  Created by Josh Campion on 26/01/2016.
//  Copyright © 2016 The Distance. All rights reserved.
//

import Foundation

/// Protocol that defines how a `KeyboardResponderInputAccessory` communicates with a `KeyboardResponder` to perform navigation.
@objc public protocol KeyboardResponderInputAccessoryDelegate {
    
    // MARK: State Queries
    
    /// Asks the delegate whether or not there is a next field to go to, to determine if the next button should be enabled or disabled.
    func inputAccessoryCanGoToNext(_ inputAccessoryView: KeyboardResponderInputAccessoryView) -> Bool
    
    /// Asks the delegate whether or not there is a previous field to go to, to determine if the next button should be enabled or disabled.
    func inputAccessoryCanGoToPrevious(_ inputAccessoryView: KeyboardResponderInputAccessoryView) -> Bool
    
    // MARK: Navigation Requests
    
    /// Asks the delegate to navigate to the next input component.
    func inputAccessoryRequestsNext(_ inputAccessoryView: KeyboardResponderInputAccessoryView)
    
    /// Asks the delegate to navigate to the previous input component.
    func inputAccessoryRequestsPrevious(_ inputAccessoryView: KeyboardResponderInputAccessoryView)
    
    /// Asks the delegate to dismiss the keyboard.
    func inputAccessoryRequestsDone(_ inputAccessoryView: KeyboardResponderInputAccessoryView)
    
}
