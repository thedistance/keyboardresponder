//
//  KeyboardResponderInputAccessoryView.swift
//  KeyboardResponder
//
//  Created by Josh Campion on 26/01/2016.
//  Copyright © 2016 The Distance. All rights reserved.
//

import Foundation

/**
 
 Defines the methods required of a `KeyboardResponderInputAccessoryView` to be used with a `KeyboardResponderInputAccessoryDelegate` and a `KeyboardResponder`.
 
 This assumes a default structure of a previous, next and done button. Each button should send actions to the `navigationDelegate`. A default implementation of this is given as `KeyboardResponderToolbar`.
 
 */
@objc public protocol KeyboardResponderInputAccessoryView {
    
    /// Flag for whether the previous button should be shown.
    var showsPrevious:Bool { get set }
    
    /// Flag for whether the next button should be shown.
    var showsNext:Bool { get set }
    
    /// Flag for whether the done button should be shown.
    var showsDone:Bool { get set }
    
    /// The image used for the previous button.
    var previousImage:UIImage? { get set }
    
    /// The title for the previous button.
    var previousTitle:String? { get set }
    
    /// The image used for the next button.
    var nextImage:UIImage? { get set }
    
    /// The title for the next button.
    var nextTitle:String? { get set }
    
    /// The image used for the done button.
    var doneImage:UIImage? { get set }
    
    /// The title for the done button.
    var doneTitle:String? { get set }
    
    /// The object to send navigation requests to.
    var navigationDelegate:KeyboardResponderInputAccessoryDelegate? { get set }
    
    /// Called to configure the items of the tool bar. Subclass desiring a new order / set of buttons should override this method, NOT calling super once the view have been created in initWithFrame, ensuring the showsPrev, showsNext and showsDone properties are observed.
    func refreshItems()
    
    /// Sets the bar button items to enabled or disabled by querying the relevant `KeyboardResponderInputAccessoryDelegate` methods.
    func refreshBarButtonItemStatus()
}